###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

include Makeconf
include Makerules

SUBDIRS = bootloader icons plymouth

all:
	for subdir in $(SUBDIRS); do \
		$(MAKE) -C $${subdir} || exit 1; \
	done

install:
	for subdir in $(SUBDIRS); do \
		$(MAKE) -C $${subdir} install || exit 1; \
	done

clean:
	for subdir in $(SUBDIRS); do \
		$(MAKE) -C $${subdir} clean || exit 1; \
	done

dist:
	git archive --format=tar --prefix=$(PACKAGE_NAME)-$(PACKAGE_VERSION)/ HEAD \
		| gzip -9 > $(PACKAGE_NAME)-$(PACKAGE_VERSION).tar.gz
